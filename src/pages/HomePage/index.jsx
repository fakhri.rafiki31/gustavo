import { useState } from "react";
import { Nav, LoginModal, RegisterModal } from "components";

import "./homepage.scss";

const HomePage = () => {
  const [isShowLogin, setShowLogin] = useState(false);
  const [isShowRegister, setShowRegister] = useState(false);

  const showLoginModal = () => {
    setShowRegister(false);
    setShowLogin(!isShowLogin);
  };

  const showRegisterModal = () => {
    setShowLogin(false);
    setShowRegister(!isShowRegister);
  };

  const hideModal = (e) => {
    if (isShowLogin && e.target.id === "loginModal") {
      setShowLogin(!isShowLogin);
    }

    if (isShowRegister && e.target.id === "registerModal") {
      setShowRegister(!isShowRegister);
    }
  };

  return (
    <div className="container" onClick={hideModal}>
      <Nav loginAction={showLoginModal} registerAction={showRegisterModal} />
      <div>
        <div className="slide-1">
          <h1>Lets Play Rock, Paper, Scissor</h1>
          <button>PLAY NOW</button>
        </div>
      </div>
      <LoginModal isShow={isShowLogin} showRegister={showRegisterModal} />
      <RegisterModal isShow={isShowRegister} showLogin={showLoginModal} />
    </div>
  );
};

export default HomePage;
