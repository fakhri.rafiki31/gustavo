const ManageDashboardPage = () => {
  return (
    <div id="tableUsers">
      <h1>Manage Users</h1>
      <table>
        <thead>
          <tr id="tableRow01">
            <th>Username</th>
            <th>Name</th>
            <th>Email</th>
            <th>Admin Role</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="tableBody01"></tbody>
      </table>
    </div>
  );
};

export default ManageDashboardPage;
