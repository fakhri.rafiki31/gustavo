import { NavAdmin } from "components";
import "./dashboardpage.scss";

const DashboardPage = ({ children }) => {
  return (
    <div className="adminpage">
      <NavAdmin />
      <main>{children}</main>
    </div>
  );
};

export default DashboardPage;
