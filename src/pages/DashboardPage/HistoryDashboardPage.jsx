const HistoryDashboardPage = () => {
  return (
    <div id="tableManage">
      <h1>Game histories</h1>
      <table>
        <thead>
          <tr id="tableRow02">
            <th>Game ID</th>
            <th>Player 1</th>
            <th>Player 2</th>
            <th>Player 1 Hero</th>
            <th>Player 2 Hero</th>
            <th>result</th>
          </tr>
        </thead>
        <tbody id="tableBody02"></tbody>
      </table>
    </div>
  );
};

export default HistoryDashboardPage;
