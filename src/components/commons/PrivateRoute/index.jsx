import {ROUTES} from "configs";
import { Redirect } from "react-router-dom";

const PrivateRoute = ({ children }) => {
  if (!localStorage.getItem("token")) return <Redirect to={ROUTES.HOMEPAGE} />;

  return children;
};

export default PrivateRoute;
