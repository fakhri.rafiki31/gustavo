import { useHistory } from "react-router-dom";
import {ROUTES} from "configs";

import "./navadmin.scss";

const NavAdmin = () => {
  const history = useHistory();

  const logout = () => {
    localStorage.removeItem("token");
    history.push(ROUTES.HOMEPAGE);
  };

  return (
    <nav className="navbar">
      <ul className="navbar__nav">
        <li className="logo">
          <span
            onClick={(e) => history.push(ROUTES.HISTORY)}
            className="nav-link"
          >
            <span className="link-text logo-text">ADMIN</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style={{
                msTransform: "rotate(360deg)",
                WebkitTransform: "rotate(360deg)",
                transform: "rotate(360deg)",
              }}
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
            >
              <path d="M15.5 5H11l5 7l-5 7h4.5l5-7z" />
              <path d="M8.5 5H4l5 7l-5 7h4.5l5-7z" />
            </svg>
          </span>
        </li>

        <li className="nav-item" id="historyNavButton">
          <span
            onClick={(e) => history.push(ROUTES.HISTORY)}
            className="nav-link"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style={{
                msTransform: "rotate(360deg)",
                WebkitTransform: "rotate(360deg)",
                transform: "rotate(360deg)",
              }}
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
            >
              <path
                d="M13 3a9 9 0 0 0-9 9H1l3.89 3.89l.07.14L9 12H6c0-3.87 3.13-7 7-7s7 3.13 7 7s-3.13 7-7 7c-1.93 0-3.68-.79-4.94-2.06l-1.42 1.42A8.954 8.954 0 0 0 13 21a9 9 0 0 0 0-18zm-1 5v5l4.28 2.54l.72-1.21l-3.5-2.08V8H12z"
                className="fa-primary"
              />
            </svg>
            <span className="link-text">History</span>
          </span>
        </li>

        <li className="nav-item" id="manageNavButton">
          <span
            onClick={(e) => history.push(ROUTES.MANAGEUSER)}
            className="nav-link"
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style={{
                msTransform: "rotate(360deg)",
                WebkitTransform: "rotate(360deg)",
                transform: "rotate(360deg)",
              }}
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
            >
              <path
                d="M7.5 6.5C7.5 8.981 9.519 11 12 11s4.5-2.019 4.5-4.5S14.481 2 12 2S7.5 4.019 7.5 6.5zM20 21h1v-1c0-3.859-3.141-7-7-7h-4c-3.86 0-7 3.141-7 7v1h17z"
                className="fa-primary"
              />
            </svg>
            <span className="link-text">Manage</span>
          </span>
        </li>

        <li className="nav-item" id="themeButton" onClick={logout}>
          <span className="nav-link">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style={{
                msTransform: "rotate(360deg)",
                WebkitTransform: "rotate(360deg)",
                transform: "rotate(360deg)",
              }}
              preserveAspectRatio="xMidYMid meet"
              viewBox="0 0 24 24"
            >
              <path d="M16 13v-2H7V8l-5 4l5 4v-3z" />
              <path
                d="M20 3h-9c-1.103 0-2 .897-2 2v4h2V5h9v14h-9v-4H9v4c0 1.103.897 2 2 2h9c1.103 0 2-.897 2-2V5c0-1.103-.897-2-2-2z"
                className="fa-primary"
              />
            </svg>
            <span className="link-text">Sign out</span>
          </span>
        </li>
      </ul>
    </nav>
  );
};

export default NavAdmin;
