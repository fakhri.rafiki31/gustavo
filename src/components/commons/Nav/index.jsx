import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ROUTES } from "configs";
import { HiUserCircle } from "react-icons/hi";
import { GetUserData } from "services";

import "./nav.scss";

const Nav = ({ loginAction = () => {}, registerAction = () => {} }) => {
  const [userMenu, setUserMenu] = useState(false);
  const [userName, setUserName] = useState("");
  const [isLogin, setIsLogin] = useState(false);
  const history = useHistory();

  const logout = () => {
    localStorage.removeItem("token");
    history.push(ROUTES.HOMEPAGE);
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      GetUserData()
        .then(({ profile: { name } }) => setUserName(name))
        .catch((err) => console.log(err));
      setIsLogin(true);
    }
  }, []);

  return (
    <header className="header">
      <h1 className="header__logo">LOGO</h1>
      <div className="header__menu">
        <ul>
          {!isLogin && (
            <>
              <li>
                <a href="/">HOME</a>
              </li>
              <li>
                <a href="game">PLAY GAME</a>
              </li>
              <li id="loginNavHeader" className="login">
                <button onClick={loginAction}>LOGIN</button>
              </li>
              <li id="signupNavHeader">
                <button onClick={registerAction}>SIGN UP</button>
              </li>
            </>
          )}
          {isLogin && (
            <li
              id="logoNavHeader"
              className="userlogo--login login"
              onMouseLeave={() => setUserMenu(false)}
              onMouseEnter={() => setUserMenu(true)}
            >
              <a href="/">
                <HiUserCircle className="icon-user" />
                <span id="userProfilNameNav">{userName}</span>
              </a>
              <div
                className={`header__dropdown ${
                  userMenu ? "displayToggle" : ""
                }`}
                id="header__dropdown"
              >
                <a href="/profil">Edit Profil</a>
                <a href="/profilstats">Your Stats</a>
                <a href="/" onClick={logout}>
                  Logout
                </a>
              </div>
            </li>
          )}
        </ul>
      </div>
    </header>
  );
};

export default Nav;
