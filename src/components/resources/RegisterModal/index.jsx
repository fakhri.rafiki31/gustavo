import {ROUTES} from "configs";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { RegisterService } from "services";

import "./registermodal.scss";

const RegisterModal = ({ isShow = false, showLogin = () => {} }) => {
  const [userData, setUserData] = useState({
    name: "",
    email: "",
    password: "",
    userName: "",
  });
  const [repeatPassword, setRepeatPassword] = useState("");
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const history = useHistory();

  const onSubmit = () => {
    if (userData.password !== repeatPassword) {
      setErrorMessage("Password did not match");
      setIsError(true);
      return false;
    }
    RegisterService(userData)
      .then(({ token }) => {
        localStorage.setItem("token", token);

        history.push(ROUTES.DASHBOARD);
      })
      .catch((e) => {
        setErrorMessage(e.data.message);
        setIsError(true);
      });
  };

  return (
    <div id="registerModal" className={`modal ${isShow ? `flex` : `none`}`}>
      <div className="modal__content">
        <h1>Sign Up</h1>
        <p>
          Already have an account?
          <span id="spanLogin" onClick={showLogin}>
            Login here
          </span>
        </p>
        <p
          id="registerMessage"
          className="errorContainer"
          style={{ display: `${isError ? "block" : "none"}` }}
        >
          {" "}
          {errorMessage}
        </p>
        <div className="modal__container">
          <label htmlFor="name">
            <b>Name</b>
          </label>
          <input
            type="text"
            placeholder="Enter Name"
            name="name"
            id="registerName"
            value={userData.name}
            onChange={(e) => setUserData({ ...userData, name: e.target.value })}
            required
          />

          <label htmlFor="username">
            <b>Username</b>
          </label>
          <input
            type="text"
            placeholder="Enter Username"
            name="username"
            id="registerUsername"
            value={userData.userName}
            onChange={(e) =>
              setUserData({ ...userData, userName: e.target.value })
            }
            required
          />

          <label htmlFor="email">
            <b>Email</b>
          </label>
          <input
            type="text"
            placeholder="Enter Email"
            name="email"
            id="registerEmail"
            value={userData.email}
            onChange={(e) =>
              setUserData({ ...userData, email: e.target.value })
            }
            required
          />

          <label htmlFor="password">
            <b>Password</b>
          </label>
          <input
            type="password"
            placeholder="Enter Password"
            name="password"
            id="registerPassword"
            value={userData.password}
            onChange={(e) =>
              setUserData({ ...userData, password: e.target.value })
            }
            required
          />

          <label htmlFor="password-repeat">
            <b>Repeat Password</b>
          </label>
          <input
            type="password"
            placeholder="Repeat Password"
            name="password-repeat"
            id="registerPassRepeat"
            value={repeatPassword}
            onChange={(e) => setRepeatPassword(e.target.value)}
            required
          />
        </div>
        <button onClick={onSubmit} className="submitBtn" id="registerButton">
          Sign Up
        </button>
        <p className="term">
          By creating an account you agree to our
          <a href="java" style={{ color: "dodgerblue" }}>
            Terms & Privacy
          </a>
          .
        </p>
      </div>
    </div>
  );
};

export default RegisterModal;
