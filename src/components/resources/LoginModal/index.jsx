import {ROUTES} from "configs";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { LoginService }from "services";

import "./loginmodal.scss";

const LoginModal = ({ isShow = false, showRegister = () => {} }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  const history = useHistory();

  const onSubmit = () => {
    LoginService(username, password)
      .then(({ token }) => {
        localStorage.setItem("token", token);

        history.push(ROUTES.DASHBOARD);
      })
      .catch((e) => {
        if (e.status === 401) {
          setErrorMessage("Check your username and password");
        } else {
          setErrorMessage(e.data.message);
        }
        setIsError(true);
      });
  };

  return (
    <div id="loginModal" className={`modal ${isShow ? `flex` : `none`}`}>
      <div className="modal__content">
        <h1>Login</h1>
        <p>
          Don't have an account?
          <span id="spanSignup" onClick={showRegister}>
            Sign up
          </span>
        </p>
        <p
          id="loginMessage"
          className="errorContainer"
          style={{ display: `${isError ? `block` : `none`}` }}
        >
          {errorMessage}
        </p>
        <div className="modal__container">
          <label htmlFor="username">
            <b>Username</b>
          </label>
          <input
            type="text"
            placeholder="Enter Username"
            name="email"
            id="loginUsername"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            required
          />

          <label htmlFor="password">
            <b>Password</b>
          </label>
          <input
            type="password"
            placeholder="Enter Password"
            name="password"
            id="loginPassword"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
        </div>
        <label className="modal__checkmark">
          remember me?
          <input type="checkbox" />
          <span className="checkmark"></span>
        </label>

        <button className="submitBtn" id="id02Submit" onClick={onSubmit}>
          Login
        </button>
      </div>
    </div>
  );
};

export default LoginModal;
