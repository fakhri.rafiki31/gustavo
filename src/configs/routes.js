const ROUTES = {
  ROOT: "/",
  HOMEPAGE: "/home",
  DASHBOARD: "/dashboard",
  MANAGEUSER: "/dashboard/user",
  HISTORY: "/dashboard/history",
};

export default ROUTES;
