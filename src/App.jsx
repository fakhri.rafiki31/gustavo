import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import { ROUTES } from "configs";

import HomePage from "pages/HomePage";
import DashboardPage from "pages/DashboardPage";
import HistoryDashboardPage from "pages/DashboardPage/HistoryDashboardPage";
import ManageDashboardPage from "pages/DashboardPage/ManageDashbordPage";
import { PrivateRoute } from "components";

function App() {
  return (
    <Router>
      <Switch>
        <Route path={ROUTES.ROOT} exact>
          <Redirect to={ROUTES.HOMEPAGE} />
        </Route>
        <Route path={ROUTES.HOMEPAGE} exact>
          <HomePage />
        </Route>
        <PrivateRoute path={ROUTES.DASHBOARD} exact>
          <Redirect to={ROUTES.HISTORY} />
        </PrivateRoute>
        <PrivateRoute path={ROUTES.HISTORY} exact>
          <DashboardPage>
            <HistoryDashboardPage />
          </DashboardPage>
        </PrivateRoute>
        <PrivateRoute path={ROUTES.MANAGEUSER} exact>
          <DashboardPage>
            <ManageDashboardPage />
          </DashboardPage>
        </PrivateRoute>
      </Switch>
    </Router>
  );
}

export default App;
