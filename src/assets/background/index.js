import BgFeatures from "./features.jpg";
import BgMain from "./main.jpg";
import BgNewsletter from "./newsletter.jpg";
import BgRequirements from "./requirements.png";
import BgRockpaperstrategy from "./rockpaperstrategy.jpg";
import BgTheGames from "./the-games.jpg";
import BgTopScore from "./top-score.png";

export {
  BgFeatures,
  BgMain,
  BgNewsletter,
  BgRequirements,
  BgRockpaperstrategy,
  BgTheGames,
  BgTopScore,
};
