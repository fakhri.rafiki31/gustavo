import SvgFacebook from "./facebook.svg";
import SvgTwitch from "./twitch.svg";
import SvgTwitter from "./twitter.svg";
import SvgVector from "./vector.svg";

export { SvgFacebook, SvgTwitch, SvgTwitter, SvgVector };
