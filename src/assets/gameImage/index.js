import ImgLogo from "./logo.png";
import ImgPaper from "./paper.png";
import ImgPerson1 from "./person1.jpg";
import ImgPerson2 from "./person2.jpg";
import ImgPerson3 from "./person3.jpg";
import ImgRefresh from "./refresh.png";
import ImgRock from "./rock.png";
import ImgScissors from "./scissors.png";
import ImgTrash from "./trash.png";
import ImgVersusLogo from "./versus-logo.jpg";
import ImgWood1 from "./wood1.jpg";
import ImgWood2 from "./wood2.jpg";

export {
  ImgLogo,
  ImgPaper,
  ImgPerson1,
  ImgPerson2,
  ImgPerson3,
  ImgRefresh,
  ImgRock,
  ImgScissors,
  ImgTrash,
  ImgVersusLogo,
  ImgWood1,
  ImgWood2,
};
