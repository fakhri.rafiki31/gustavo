import axios from "axios";

const LoginService = (username, password) =>
  new Promise((resolve, reject) =>
    axios
      .post(`${process.env.REACT_APP_ROOT_URL}/login`, {
        userName: username,
        password,
      })
      .then((data) => resolve(data.data))
      .catch(({ response }) => reject(response))
  );

export default LoginService;
