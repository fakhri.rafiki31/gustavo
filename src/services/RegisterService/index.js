import axios from "axios";

const RegisterService = (user) =>
  new Promise((resolve, reject) =>
    axios
      .post(`${process.env.REACT_APP_ROOT_URL}/users`, {
        userName: user.userName,
        password: user.password,
        email: user.email,
        name: user.name,
      })
      .then((data) => resolve(data.data))
      .catch(({ response }) => reject(response))
  );

export default RegisterService;
