import axios from "axios";

const GetUserData = () =>
  new Promise((resolve, reject) => {
    const USER_TOKEN = localStorage.getItem("token");
    const AuthStr = "Bearer ".concat(USER_TOKEN);
    axios
      .get(`${process.env.REACT_APP_ROOT_URL}/userprofiles`, {
        headers: { Authorization: AuthStr },
      })
      .then((data) => resolve(data.data))
      .catch(({ response }) => reject(response));
  });

export default GetUserData;
